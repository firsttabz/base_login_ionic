import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { WindowManagerProvider } from '../../providers/window-manager/window-manager';
import { ApiProvider } from '../../providers/api/api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private wm: WindowManagerProvider,
    private ps: ApiProvider) {
    this.presentLoginModal();
  }

  private presentLoginModal() {
    console.log('Login modal executing');
    let loginModal = this.modalCtrl.create(LoginPage, null, { enableBackdropDismiss: false });
    loginModal.onDidDismiss(() => {
      console.log("Logeo existoso")
    });
    loginModal.present();
  }


}
