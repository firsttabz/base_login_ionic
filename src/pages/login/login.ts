import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { WindowManagerProvider } from '../../providers/window-manager/window-manager';
import { ApiProvider } from '../../providers/api/api';
import validator from 'validator';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public login_data;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl:ViewController,
              private wm:WindowManagerProvider,
              private api:ApiProvider) {
    this.login_data = {
      email: '',
      password: '',
      url:'',
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  public btn_click_login(){
    console.log("btn_click_login()");
    console.log(this.login_data)
    let band = true;
    let err ="";
    /*if(!validator.isURL(this.login_data.url,{
      protocols: ['http','https'],
    })){band =false;err = "El formato URL es incorrecto.";}*/
    if(!validator.isEmail(this.login_data.email)){band =false;err = "Tu usuario está mal escrito o está vacío.";}
    if(validator.isEmpty(this.login_data.password)){band =false;err = "Tu campo password está vacío.";}
    if(validator.isEmpty(this.login_data.email)){band =false;err = "Tu campo email está vacío.";}
    if(validator.isEmpty(this.login_data.url)){band =false;err = "Tu campo URL está vacío.";}

    if(!band){
      this.wm.presentAlert("Error",err);
    }
    else{
      this.wm.presentLoading("Espere un momento...", (loadingCtrl) => {
        /*
        *Aqui iniciar sesion y recuperar los datos del vendedor
        */
        this.api.login(this.login_data.email,this.login_data.password,this.login_data.url,(res) => {
          loadingCtrl.dismiss();
          if(res.success == true){
            this.viewCtrl.dismiss();
          }
        });
        ///////////////////////////////////////////////////
      });
    }
  }
}
