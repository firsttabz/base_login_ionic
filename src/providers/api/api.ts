import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WindowManagerProvider } from '../window-manager/window-manager';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient, private wm:WindowManagerProvider) {
    console.log('Hello ApiProvider Provider');
  }

  login(user,password,url, __callback){
    this.http.post(url+"/api/v1/login",{user,password})
    .subscribe((res) => {
      console.log(res)
      __callback(res);
    },(err) => {
      console.log(err);
      __callback({success:false})
      this.wm.presentAlert("Error!","Ocurrió un error al tratar de loguearse.");
    });
  }
}
