import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { LoadingController, AlertController, ToastController } from 'ionic-angular';
import { ApplicationRef } from '@angular/core';
import { PopoverController } from 'ionic-angular';
/*
  Generated class for the WindowManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WindowManagerProvider {

  constructor(public http: Http,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private popoverCtrl: PopoverController, 
    private applicationRef : ApplicationRef
  ) {
    console.log('Hello WindowManagerProvider Provider');
  }

  public presentLoading(content, callback) {
    let dismissed = false;
    let loading = this.loadingCtrl.create({
      content: content
    });

    loading.present();
    if (callback) {
      dismissed = true;
      callback(loading);
    }
    setTimeout(() => {
      if (!dismissed) {
        loading.dismiss();
        this.presentAlert('Error', 'Ocurrió un error en la red');
      }
    }, 30000);
  }

  public presentAlert(title, message, buttons?) {
    buttons = (!buttons) ? ['OK'] : buttons;
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: buttons,
      enableBackdropDismiss:false
    });
    alert.present();
  }

  public presentAlertOptions(title, message, inputs, buttons?) {
    buttons = (!buttons) ? ['OK'] : buttons;
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      inputs: inputs,
      buttons: buttons,
      enableBackdropDismiss:false
    });
    alert.present();
  }

  public presentToast(message, time = 3000, position = "bottom") {
    let toast = this.toastCtrl.create({
      message: message,
      duration: time,
      position: position
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  public presentPopover(Event,Page, _callback) {
    let popover = this.popoverCtrl.create(Page);
    popover.present({
      ev: Event
    });
    popover.onDidDismiss(data =>{
      _callback(data)
    });
  }

  public forceVarsBindings(){
    this.applicationRef.tick();
  }
  public presentErrorNotSupported() {
    this.presentAlert("Error", "Implementación aun no soportada.");
  }
}
